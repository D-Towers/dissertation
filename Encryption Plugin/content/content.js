$(window).on('load', function(){
	setTimeout(highlight, 1000);
});

//Scans elements and gives appropriate classes
function highlight(){
	var arr = $("span, div").not(".nothing, .PERSA, .PEAES, .editing");
	arr.each(function() {
		//Identifies RSA keys
		if (!$(this).hasClass("PERSA") && $(this).clone().children().remove().end().text().replace(/(\r\n|\n|\r)/gm, "").match(/-----BEGIN PUBLIC KEY-----.*?-----END PUBLIC KEY-----/) != null){
			$(this).attr("title", "Click to add RSA Key");
			$(this).css("text-decoration", "underline");
			$(this).css("text-decoration-color", "#ffdb68");
			$(this).addClass("PERSA");
		} 
		//Identifies AES Keys
		else if (!$(this).hasClass("PEAES") && $(this).clone().children().remove().end().text().replace(/(\r\n|\n|\r)/gm, "").match(/-----BEGIN AES KEY-----.*?-----END AES KEY-----/) != null){
			$(this).attr("title", "Click to add AES Key")
			$(this).css("text-decoration", "underline ");
			$(this).css("text-decoration-color", "#92D050");
			$(this).addClass("PEAES");
		}
		//Identifies text entry fields, so the plugin will not decrypt text inside
		else if ($(this).prop('contenteditable') == "true" ) {
			$(this).addClass('editing');
			$(this).removeClass('AESCT');
			$(this).find("*").addClass('editing');
			$(this).find("*").removeClass('AESCT');
		}
		else {
			if($(this).parent().hasClass("editing")){
				$(this).addClass('editing');
			}
			//Identifies encrypted messages
			else if (!$(this).hasClass("AESCT") &&  !$(this).hasClass("editing") && $(this).clone().children().remove().end().text().replace(/(\r\n|\n|\r)/gm, "").includes("-----BEGIN ENC MSG-----") && $(this).clone().children().remove().end().text().replace(/(\r\n|\n|\r)/gm, "").includes("-----END ENC MSG-----")){
				$(this).addClass("AESCT");
			}
			//Adds nothing class so elements are only scanned once
			else {
				$(this).addClass("nothing");
			}
		}
		
		PERSA();
		PEAES();
		AESCT();
	});
	
	//repeats function every tenth of a second
	setTimeout(highlight, 100);
}

//Stores RSA keys clicked on
function PERSA() {
	$(".PERSA").off("click");
	$(".PERSA").on("click", async function(){
		key =  prompt("Please enter an identifier for this key:");
		if (key != null && key != "") {
			var txt = $(this).clone().children().remove().end().text().replace(/(\r\n|\n|\r)/gm, "");
			await chrome.storage.local.get(["rsakeys"], function(result){
				var rsakeys = result.rsakeys;
				var used = false;
				for (var i = 0; rsakeys != null && i < rsakeys.length; i += 2){
					if (rsakeys[i] == key)
						used = true;
				}
				if (rsakeys != null && used == true) {
					alert('Name in use!');
				}
				else {
					if (rsakeys == null)
						rsakeys = [];
					rsakeys[rsakeys.length] = key.substring(key.search("-----BEGIN PUBLIC KEY-----"), key.search("-----END PUBLIC KEY-----") + 24);
					rsakeys[rsakeys.length] = txt;
					$(this).children().text("Key Added! :)"); 
				}
				alert("Public Key stored");
				chrome.storage.local.set({"rsakeys": rsakeys});
			});
		}
	});
}

//decrypts and store clicked on AES keys
function PEAES() {
	$(".PEAES").off("click");
	$(".PEAES").on("click",  async function(){
		key =  prompt("Please enter an identifier for this key:");
		if (key != null && key != "") {
			var txt = $(this).clone().children().remove().end().text();
			await chrome.storage.local.get(["aeskeys"], async function(result){
				var aeskeys = result.aeskeys;
				var used = false;
				for (var i = 0; aeskeys != null && i < aeskeys.length; i += 2){
					if (aeskeys[i] == key)
						used = true;
				}
				new Promise(function(resolve, reject){
					if (aeskeys != null && used == true) {
						alert('Name in use!');
					}
					else {
						if (aeskeys == null)
							aeskeys = [];
						aeskeys[aeskeys.length] = key;
						var aesKey = txt.substring(txt.search("-----BEGIN AES KEY-----") + 23, txt.search("-----END AES KEY-----"));
						chrome.storage.local.get(["sk"], function(result) {
							var sk = forge.pki.privateKeyFromPem(result.sk);
							aesKey = sk.decrypt(forge.util.decode64(aesKey));
							aeskeys[aeskeys.length] = aesKey;
							resolve(aeskeys);
						});
					}
				}).then(function(aeskeys){
					alert("AES Key stored");
					chrome.storage.local.set({"aeskeys": aeskeys});
				});
			});
		}
	});
}

//Decryption function
function AESCT() {
	new Promise(function(resolve, reject){
		chrome.storage.local.get(["selected"], function(result){
			//if a key is selected, decrypt message.
			if(result.selected != null) {
				chrome.storage.local.get(["aeskeys"], async function(bob){
					var aeskeys = bob.aeskeys;
					for (var i = 0; i < aeskeys.length; i+=2){
						if (aeskeys[i] == result.selected.trim()) {
							resolve(forge.util.decode64(aeskeys[i+1]));
							break;
						}
					}
				});
			}
		});
	})
	.then(function(key) {
		$(".AESCT").each(async function(){
			var obj = $(this);
			new Promise(function(resolve, reject){
				var string = obj.clone().children().remove().end().text()
				var startLoc = string.search("-----BEGIN ENC MSG----IV: ");
				var iv = forge.util.decode64(string.substring( startLoc + 27, startLoc + 51));
				var cipherText = forge.util.decode64(obj.clone().children().remove().end().text().substring(startLoc + 57, string.search("-----END ENC MSG-----")));
				resolve(aesDecryption(key, iv, cipherText));
			}).then(function(clear){
				obj.text(obj.text().replace(/-----BEGIN ENC MSG-----.*?-----END ENC MSG-----/g, clear.data));
				//remove class so element won't be checked
				obj.removeClass("AESCT");
			});
		})
	});
}

//Function called to decrypt encrypted messages
async function aesDecryption(key, iv, cipherText) {
	var cipher = forge.cipher.createDecipher('AES-CBC', key);
	cipher.start({iv: iv}); /*34*/
	cipher.update(forge.util.createBuffer(cipherText));
	cipher.finish();
	return await cipher.output;
}