//Number of buttons before box needs to be increased
var places = 3;

var defHeight = 220;

//amount popup needs to increase/decrease by for each button added
var incSize = 30;

var sideSpace = 160;

$(document).ready(function() {
	//Function to input with function that checks if the text-box needs to increase in size 
	$('#entry').bind("input propertychange", function(){

		//Calls function to check text
		textCheck();

		//Checks whether after an update to the text box it has text inside it
		if ($("#entry").text() == null || $("#entry").text() == "") {
			//If yes check if the encrypt button already had the back arrow, if yes do nothing, if not set it to the arrow
			if($("#encrypt").text() != "&#8594;") {
				anim($("#encrypt"), "&#8594", 500);
			}
		}
		else {
			//If not check if the encrypt button already says "encrypt", if yes do nothing, if not set it to "Encrypt"
			if($("#encrypt").text() != "Encrypt") {
				anim($("#encrypt"), "Encrypt", 500);
			}
		}
	});

	//Due to using both a span and div to animate the text-box, this function will change focus to the span when the user clicks on the div 
	$('#ta').click(function(){
		$('#entry').focus();
	});

	//function is ran when plugin menu is opened, to have correct AES Key selected
	chrome.storage.local.get(["selected"], function(result){
		if(result.selected != null) {
			selected(result.selected);
		}
	});

	//function for generating RSA key pairs
	$('#rsa').click(rsaGen);

	//function to copy public key into clipboard
	$('#copy').click(rsaCopy);

	//Function used to generate and securely distribute AES Keys
	$('#aes').click(aesGen);

	setClick();

	//function to return the plugin to the main menu, after selecting from one of the sub menus
	$('#back').click(function() {
		new Promise(function(resolve, reject) {
			$("#aligner, #options").animate({'margin-left': "+=" + sideSpace});
			$(" body, html").animate({"height": defHeight});
			resolve();
		})

		.then(function(){
			setTimeout(function(){$(".option").remove();}, 300);
		});
	});

	//Function to encrypt text in text-box
	$('#encrypt').click(function() {
		//If there is nothing in the text box, the button will send the user back to the main menu
		if ($("#entry").text() == null || $("#entry").text() == "") {
			enterText("-");
		}
		//Else the text will be encrypted
		else {
			chrome.storage.local.get(["selected"], function(result){
				var idi  = result.selected.trim();
				if(idi != null) {
					var clearText = $("#entry").text();
					new Promise(function(resolve, reject) {
						chrome.storage.local.get(["aeskeys"], async function(bob){
							var aeskeys = bob.aeskeys;
							for (var i = 0; i < aeskeys.length; i+=2){
								if (aeskeys[i] == idi) {
									resolve(aeskeys[i+1]);
									break;
								}
							}
						});
					})
					//Then stored in the correct format
					.then(function(theKey){
						var ivi;
						new Promise(function(resolve, reject){
							resolve(forge.random.getBytesSync(16));
						})
						.then(async function(iv){
							ivi = iv;
							var cipherText = await aesEncryption(forge.util.decode64(theKey), ivi, clearText);
							var msg = "-----BEGIN ENC MSG-----IV: " + forge.util.encode64(ivi) + " msg: " + forge.util.encode64(cipherText.data) + "-----END ENC MSG-----";
							anim($("#encrypt"), "Copied", 500);
							await setTimeout(function(){
								$("#encrypt").css({"color": "#92d050"});
							}, 1800);
							anim($("#encrypt"), ($("#entry").text() == null  || $("#entry").text() == "") ? "&#8594;" : "Encrypt" , 2300);
							navigator.clipboard.writeText(msg);
						});
					});
				}
			});
		}
	});

	//Will run tests
	$("#logo").click(function() {
		chrome.tabs.create({"url" : chrome.extension.getURL('../tests/testRunner.html')})
	});
});

//Function used to encrypt
async function aesEncryption(key, iv, plainText){
	var cipher = forge.cipher.createCipher('AES-CBC', key);
	cipher.start({iv: iv});
	cipher.update(forge.util.createBuffer(plainText, "utf8"));
	cipher.finish();
	return await cipher.output
}

//Function clears selected key and resets onClick function of button
function closing() {
	$("#decrypt").animate({"width": "120"});
	$("#decrypt").css({"color": "#92d050"});
	$("#decrypt").off("click");
	setClick();
	$("#close").animate({"width": "0"});
	setTimeout(function(){
		$("#decrypt").html("Set AES Key");
		$("#decrypt").css({"color": "white"});
		$("#close").remove();
		chrome.storage.local.set({"selected": null});
	}, 500);
}

//Switches between main menu and text entry menu depending on operator
function enterText(operator) {
	$("#aligner, #textarea").animate({'margin-left': operator + "=" + sideSpace});
}

//function applies necessary CSS to change to side menu
function sideMenu(buttons) {
	new Promise(function(resolve, reject){
		var newButtons = buttons.length/2 - places;
		if (newButtons > 0) {
			var calc = defHeight + newButtons * incSize;
			$(" body ").animate({'height': calc});
		}
		resolve(buttons);
	})
	.then(function(buttons){
		for (var i = 0; i < buttons.length; i += 2) {
			$("#options").append("<button id=" + buttons[i] + " class='button option' type='button' title=" + buttons[i] + "> " + buttons[i] + " </button>");
			//Keeps box at correct height, until ready to smoothly animate the height change
			$("body").css("height", defHeight);
		}
	})
	.then(function(){
		$("#aligner, #options").animate({'margin-left': "-=" + sideSpace});
	});
}

//When key is selected it sets the appropriate on click events
function selected(key){
	$("#decrypt").html(key);
	$("#decrypt").css({"width": "95"});
	$("#decrypt").off("click");
	$("#decrypt").on("click", function(){enterText("+")});
	$("#aligner").append("<button id='close' type='button' title='Turn off auto Decryption'> X </button>");
	$("#close").click(closing);
}

//Checks if the size of the text box needs to increase
function textCheck() {
	if( $("#entry").height() > $("#ta").height()){
		var dvd = parseInt(($("#entry").height() - $("#ta").height()) / 16) + 1;
		$("#ta").animate({"height": "+=" + 32 * dvd});
		$("body").animate({"height": "+=" + 32 * dvd});
	}
}

//Function to set the on click event for the "Set AES Key" button.
function setClick() {
	$('#decrypt').on("click", function decClick() {
		var promise = new Promise(function (resolve, reject) {
			chrome.storage.local.get(["aeskeys"], function (result) {
				var aeskeys = result.aeskeys;
					resolve(aeskeys);
			});
		})
		.then(async function (value) {
			if (value == null){
				userAlert("Please store an AES key before attempting to set one");
			}
			else {
				await sideMenu(value);
				return value;
			}
		})
		.then(function (value) {
			new Promise(function( resolve, reject) {
				$(".option").click(function () {
					selected($(this).html())
					chrome.storage.local.set({"selected": $(this).html()});
					resolve();
				});
			})
			.then(function(){
				$("#back").trigger("click");
			});
		});
	});
}

//Used for text animaiton
async function anim(target, msg, time){
	await target.css({"color": "#92d050"});
	await setTimeout(function(){
		$("#encrypt").html(msg);
		$("#encrypt").css({"color": "white"});
	}, time);
}

//Sends an alert to the user
function userAlert(msg){
	chrome.tabs.executeScript({code: "alert('" + msg + "');"});
	return msg;
}

//Generates RSA keypair
function rsaGen() {
	return new Promise(function (resolve, reject){
		forge.pki.rsa.generateKeyPair({bits: 2048, workers: 2}, async function (err, keypair) {
			var pk = await keypair.publicKey;
			await chrome.storage.local.set({ pk: forge.pki.publicKeyToPem(pk), sk: forge.pki.privateKeyToPem(keypair.privateKey)});
			resolve(pk);
		});
	})
	//Copies the resultant Public Key to the clipboard so the user doesn't need to click the copy button
	.then(async function(result){
		await navigator.clipboard.writeText(forge.pki.publicKeyToPem(result));
		userAlert("New Public/Private Key Pair generated, and copied to Clipboard");
	});
}

//retrieves public key 
async function rsaCopy() {
	var res = null;
	await new Promise(async function (resolve, reject) {
		chrome.storage.local.get(["pk"], async function (result) {
			if (result.pk == null)
				reject();
			else
				resolve(result.pk);
		})
	})
	//If the promise resolves, key is copied to the clipboard, then user is alerted that the key has been copied
		.then(async function (result) {
			res = result;
			await navigator.clipboard.writeText(res.replace(/(\r\n|\n|\r)/gm, ""));
			userAlert("Public Key copied to Clipboard");
		})
		//If the promise is rejected (no previously genned key) user will be told to generate a key
		.catch(async function () {
			userAlert("No Key found, please generate key");
		});
	return res;
}

//Calls key generation function after public key has been retrieved
 function aesGen() {
	var promise = new Promise(function (resolve, reject) {
		//Retrieves the list of RSA Keys
		chrome.storage.local.get(["rsakeys"], async function (result) {
			var rsakeys = result.rsakeys;
			//If the list is not empty add key ids to buttons on side menu
			if(rsakeys != null) {
				await sideMenu(rsakeys);
				resolve(rsakeys);
			}
			//If the list is empty, alert the user
			else
				userAlert("No Public Keys stored to encrypt aeskey with, please obtain public key of intended recipient of aeskey");
		});
	})
	//If the list is not empty set the buttons to generate an AES Key encrypted with the stored public key
	.then(function (rsakeys) {
		$(".option").click( async function () {
			//For every rsakey go through each identifier to find the match with the clicked button
			for (var i = 0; rsakeys != null && i < rsakeys.length; i += 2) {
				//If the Identifier matches generate the key and encrypt
				if (rsakeys[i] === $(this).attr('id')) {
					aesGenKey($(this).attr('id'), rsakeys[i+1]);
				};
			}
		});
	});
}
	
//Generates an aes key
function aesGenKey(name, pk){
	pk = forge.pki.publicKeyFromPem(pk);
	var num = 1;
	var key64 = forge.util.encode64(forge.random.getBytesSync(16));
	var encKey = "-----BEGIN AES KEY-----" + forge.util.encode64(pk.encrypt(key64)) + "-----END AES KEY-----";
	navigator.clipboard.writeText(encKey);
	var used = false;
	var promise = new Promise(function(resolve, reject){
		chrome.storage.local.get(["aeskeys"], async function(result){
			var aeskeys = result.aeskeys;
			for (var j = 0; aeskeys != null && j < aeskeys.length; j += 2){
				if (aeskeys[j] == name){
					name = name + num;
					num++;
					j = 0;
				}
			}
			resolve(aeskeys);
		});
	}).then(function(aeskeys){
		if (aeskeys != null && used == true) {
			userAlert("Name in use!");
		}
		else {
			if (aeskeys == null){
				aeskeys = [];
			}
			aeskeys[aeskeys.length] = name;
			aeskeys[aeskeys.length] = key64;
			return aeskeys;
		}
	})
	.then(function(aeskeys){
		chrome.storage.local.set({"aeskeys": aeskeys});
		userAlert("AES key added to clipboard");
	});
	return encKey;
}