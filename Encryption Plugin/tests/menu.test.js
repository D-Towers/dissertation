var assert = chai.assert;

describe('RSA',  function() {
	describe('Without generating a key', function() {
		before(function(){
			chrome.storage.local.clear();
		});
		
		it('Key should be null',  async function(){
			assert.equal(await rsaCopy(), null);
		});
	});
		
	describe('Generating a Key prior', function() {
		let pk;
		let sk;
		before(async function() {
			await rsaGen();
			pk = await rsaCopy();
			pk = pk.replace(/(\r\n|\n|\r)/gm, "");
			await chrome.storage.local.get(["sk"], function(res){
				 sk = res.sk;
			});
		});
		
		it('Public key should not be null',  async function(){
			assert.notEqual(pk, null);
		});
		
		it('Private key should not be null',  async function(){
			assert.notEqual(sk, null);
		});
		
		it('Should create a new key', async function(){
			await rsaGen();
			assert.notEqual(await rsaCopy(), pk);
		});
		
		it('Key should be in correct format', function(){			
			assert.notEqual(null, pk.match(/-----BEGIN PUBLIC KEY-----.*?-----END PUBLIC KEY-----/));
		});
		
		it('Should encrypt a message not decryptable by a non-associated private key', async function(){
			pubk = forge.pki.publicKeyFromPem(pk);
			let plainText = "test";
			let cipherText = pubk.encrypt(plainText);
			assert.equal(forge.pki.privateKeyFromPem(sk).decrypt(cipherText), plainText);
		});
	});
});

describe('AES', function() {
	let keyList;
	describe('Before generation', function(){
		before(async function() {
			await chrome.storage.local.get(["aeskeys"], function(result) {
				keyList = result.aeskeys;
			});
		});
		
		it('List should initially be null', function() {
			assert.equal(keyList, null);
		});
	});
	
	describe('After generation', function() {
		let pk;
		let sk;
		//Encrypted Key before being stripped of identifiers
		let forKey;
		//Encrypted Key after being stripped of identifiers
		let encKey;
		//Encrypted key after being decrypted
		let decKey;
		let name = "test";
		//Key stored locally
		let aes;
		before(async function(){
			chrome.storage.local.clear();
			await rsaGen();
			pk = await rsaCopy();
			forKey = await aesGenKey(name, pk);
			await chrome.storage.local.get(["sk"], async function(result){
				sk = await forge.pki.privateKeyFromPem(result.sk);
				encKey = forKey.replace(/-----.{3,5} AES KEY-----/g, "");
				decKey = await sk.decrypt(forge.util.decode64(encKey));
			});
		});

		describe('Unencrypted AES key (the key stored for the user)', function() {
			
			before(async function() {
				chrome.storage.local.get(["aeskeys"], async function(result) {
					keyList = await result.aeskeys;
					for (var i = 0; i < keyList.length; i += 2){
						if( keyList[i] === name) {
							aes = await keyList[i + 1];
						}	
					}
				});
			});
			
			it('Generated Key should not be null', async function(){
				await assert.notEqual(await aes, null);
			});
			
			it('Key generated should have name of Public Key used in function', function(){
				for(var i = 1; i < keyList.length; i += 2) {
					if(keyList[i] == aes) {
						assert.equal(keyList[i-1], name);
						break;
					}					
				}
			});
			
			describe ('If another key is generated', async function(){
				let newEnc;
				let newDec;
				let newName;
				before(async function(){
					newEnc = await aesGenKey(name, pk);
				});
				
				describe("", function() {
					before(async function(){
						await chrome.storage.local.get(["aeskeys"], async function(res){
							keyList = await res.aeskeys;
						});
						
						 chrome.storage.local.get(["sk"], async function(result){
							sk =  forge.pki.privateKeyFromPem(result.sk);
							newEnc = newEnc.replace(/-----.{3,5} AES KEY-----/g, "");
							newDec =  sk.decrypt(forge.util.decode64(newEnc));
							for(var i = 1; i < keyList.length; i += 2) {
								if(keyList[i] == newDec) {
									newName = keyList[i-1];
									break;
								}					
							}
							
						});
					});
					
					it('If another key is generated with the same name, it should have an number appended', async function(){
						 assert.equal(await newName, name + 1);
					});
				});
			});
			
			it('should be able to encrypt a message and decrypt it', async function(){
				var message = "This is a Test";
				var iv = forge.random.getBytesSync(16);
				var cipherText = await aesEncryption(aes, iv, message);
				var plainText = await aesDecryption(aes, iv, cipherText);
				assert.equal(message, plainText.data);
			});
		});
		
		describe('Encrypted AES key',  function() {
			it('should not be null', function(){
				assert.notEqual(encKey, null);
			});
			
			it('Should be in the correct format before decryption', function (){
				assert.notEqual(null, forKey.match(/-----BEGIN AES KEY-----.*?-----END AES KEY-----/));
			});
			
			describe('After Key has been decrypted', function(){
				it('should not be null', async function() {
					assert.notEqual(decKey, null);
				});
				
				it('should be equal to the stored key', function(){
					assert.equal(aes, decKey);
				});
				
				it('should be able to encrypt and decrypt a message', async function() {
					var message = "This is a Test";
					var iv = forge.random.getBytesSync(16);
					var cipherText = await aesEncryption(decKey, iv, message);
					var plainText = await aesDecryption(decKey, iv, cipherText);
					assert.equal(message, plainText.data)
				});
			});
		});
		
		describe('Both Keys', function() {
			
			var iv;
			var cipherTextDec;
			var cipherTextSto;
			var message = "This is a Test";
			before(async function() {
				iv = forge.random.getBytesSync(16);
				cipherTextDec = (await aesEncryption(decKey, iv, message)).data;
				cipherTextSto = (await aesEncryption(aes, iv, message)).data;
			});
			
			it('Given the same IV and Plaintext they should generate the same cipher text', function(){
				assert.equal(forge.util.encode64(cipherTextSto), forge.util.encode64(cipherTextDec));
			});
			
			describe('Each key should be able to decrypt a message encrypted by the other', function(){
				it('The Encrypted Key should be able to decrypt messages encrypted by the stored key', async function() {
					var plainText = await aesDecryption(decKey, iv, cipherTextSto);
					assert.equal(message, plainText.data);
				});
				
				it('The Stored Key should be able to decrypt messages encrypted by the encrypted key', async function() {
					var plainText = await aesDecryption(aes, iv, cipherTextDec);
					assert.equal(message, plainText.data);
				});
			});
		});
	});
	
});